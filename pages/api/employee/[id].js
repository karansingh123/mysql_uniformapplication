import { createRouter } from "next-connect";
import onError from "../../../src/common/errormiddleware";
import {
  getSchoolById,
  deleteSchoolById,
  updateSchool,
} from "../../../src/controller/employee/employee";

const handler = createRouter();
handler.get(getSchoolById);
handler.delete(deleteSchoolById);
handler.put(updateSchool);
export default handler.handler();
