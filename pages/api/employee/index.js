import nc, { createRouter } from "next-connect";
import onError from "../../../src/common/errormiddleware";
import {
  getAllSchools,
  saveSchool,
} from "../../../src/controller/employee/employee";
const handler = createRouter();
handler.get(getAllSchools);
handler.post(saveSchool);
export default handler.handler();
