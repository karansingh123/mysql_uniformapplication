import EditSchool from "../../src/editSchools/editSchool";

function updateSchool({ employee }) {
  console.log("employee", employee);
  return <EditSchool employeeUpdateData={employee} />;
}

export async function getServerSideProps({ params }) {
  const res = await fetch(`http://localhost:3000/api/employee/${params.id}`);
  const employee = await res.json();

  return {
    props: { employee },
  };
}

export default updateSchool;
