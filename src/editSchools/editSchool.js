import axios from "axios";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import styles from "../styles/UpdateSchool.module.css";
import { Button, Col, Container, Image, Modal, Row } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";

function EditSchool(props) {
  const {
    showModalA,
    handleCloseModals,
    employeeUpdateData } = props
  console.log("employeeid", employeeUpdateData);
  const router = useRouter();
  const [addSchool, setSchool] = useState({
    name: "",
    email: "",
    address: "",
    contact: "",
    state: "",
    city: "",
  });
  console.log(addSchool, 'oooooooooooooooooooooo')

  useEffect(() => {
    setSchool(employeeUpdateData);
  }, [employeeUpdateData]);

  const onSubmit = async (e) => {
    e.preventDefault();
    let data = await axios.put(
      `http://localhost:3000/api/employee/${employeeUpdateData.id}`,
      addSchool
    );
    toast.success('Data Successfully Updated', {
      position: "top-right",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
    });
    if (data.data) router.push("/"); handleCloseModals()
    setSchool({
      name: "",
      email: "",
      address: "",
      contact: "",
      state: "",
      city: "",
    });
  };

  const handleChange = (e) => {
    const value = e.target.value;
    console.log("value", value);
    setSchool({ ...addSchool, [e.target.name]: value });
  };
  return (
    <>
      {/* <div className={styles.addform}>
        <h1>ADD EMPLOYEE</h1>
        <form onSubmit={onSubmit}>
          <div>
            <input
              type="text"
              className={styles.input}
              name="name"
              placeholder="Enter Name"
              onChange={handleChange}
              value={addSchool.name}
            />
          </div>
          <div>
            <input
              type="email"
              className={styles.input}
              name="email"
              placeholder="Enter Email"
              onChange={handleChange}
              value={addSchool.email}
            />
          </div>
          <div>
            <input
              type="text"
              className={styles.input}
              name="address"
              placeholder="Enter Address"
              onChange={handleChange}
              value={addSchool.address}
            />
          </div>
          <div>
            <input
              type="text"
              className={styles.input}
              name="contact"
              placeholder="Enter Phone"
              onChange={handleChange}
              value={addSchool.contact}
            />
          </div>
          <div>
            <input
              type="text"
              className={styles.input}
              name="state"
              placeholder="state"
              onChange={handleChange}
              value={addSchool.state}
            />
          </div>
          <div>
            <input
              type="text"
              className={styles.input}
              name="city"
              placeholder="City"
              onChange={handleChange}
              value={addSchool.city}
            />
          </div>
          <div>
            <button type="submit" className={styles.button}>
              Submit
            </button>
            <button className={styles.button}>
              <Link href={`/`}>Go Back</Link>
            </button>
          </div>
        </form>
      </div> */}
      <Modal
        show={showModalA}
        onHide={handleCloseModals}
        lg="true"
        contentClassName={`${styles.addform}`}
        dialogClassName={``}
      >
        <Modal.Header className={`px-3 pb-0 border-0 d-block d-md-flex justify-content-center justify-content-sm-end`}>
          <Col md={12} className={` d-flex justify-content-end`}>
            <div role="button" onClick={() => handleCloseModals()}>
              <Image src="/modalCrossIcon.svg" height={20} width={20} />
            </div>
          </Col>
        </Modal.Header>
        <Modal.Body className={`px-3 ${styles.scrollarz}`}>
          <Container>
            <form onSubmit={onSubmit}>
              <Row>
                <Col md={12} className={`textStart d-flex justify-content-center`}>
                  <div>
                    <h1>ADD Schools</h1>

                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="name"
                      placeholder="Enter Name"
                      onChange={handleChange}
                      value={addSchool.name}
                    />
                    <label htmlFor="email">Name</label>
                  </div>
                </Col>

                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="email"
                      className={styles.input}
                      name="email"
                      placeholder="Enter Email"
                      onChange={handleChange}
                      value={addSchool.email}
                    />
                    <label htmlFor="email">Email</label>
                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="address"
                      placeholder="Enter Address"
                      onChange={handleChange}
                      value={addSchool.address}
                    />
                    <label htmlFor="email">Address</label>
                  </div>
                </Col>


                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="contact"
                      placeholder="Enter Phone"
                      onChange={handleChange}
                      value={addSchool.contact}
                    />
                    <label htmlFor="email">Phone</label>
                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="state"
                      placeholder="state"
                      onChange={handleChange}
                      value={addSchool.state}
                    />
                    <label htmlFor="email">State</label>
                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="city"
                      placeholder="City"
                      onChange={handleChange}
                      value={addSchool.city}
                    />
                    <label htmlFor="email">City</label>
                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      id="uploadBtn" type="file" name="image" className={styles.input} multiple="multiple" placeholder="No File Chosen"
                    />
                  </div>
                </Col>

              </Row>
              <div>
                <Button type="submit" className={styles.button}>
                  Submit
                </Button>
                <Button className={styles.button} onClick={() => handleCloseModals()}> Go Back</Button>
              </div>
            </form>
          </Container>
        </Modal.Body>
      </Modal>
      <ToastContainer />
    </>
  );
}

export default EditSchool;
