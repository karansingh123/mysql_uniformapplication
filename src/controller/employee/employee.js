import { executeQuery } from "../../config/db";
import employeeValidation from "../../common/employeeValidator";
import ErrorHandler from "../../common/errorHandler";
const getAllSchools = async (req, res) => {
  try {
    console.log("all the employees");
    let employeeData = await executeQuery("select * from schools", []);
    res.send(employeeData);
  } catch (err) {
    res.status(500).json(err);
  }
};

const getSchoolById = async (req, res, next) => {
  let id = req.query.id;
  try {
    console.log("employee by id");
    let employeeData = await executeQuery(
      `select * from schools where id=${id}`,
      []
    );
    if (employeeData.length > 0) res.status(200).json(employeeData);
    else {
      next(new ErrorHandler(`no employee found with this id ${id}`, 404));
    }
  } catch (err) {
    res.status(500).json(err);
  }
};

const deleteSchoolById = async (req, res, next) => {
  let id = req.query.id;
  try {
    let employeeData = await executeQuery(
      "delete from schools where id=?",
      [id]
    );
    res.status(200).json("School Deleted Successfully");
  } catch (err) {
    res.status(500).json(err);
  }
};

const saveSchool = async (req, res) => {
  try {
    const result = req.body;
    const { name, email, address, contact,city,state } = result;
    let { error } = employeeValidation(result);
    if (error) {
      res.status(400).json(error.details[0].message);
    } else {
      console.log("post request");
      let employeeData = await executeQuery(
        "insert into schools(name, email, address, contact,city,state) values(?,?,?,?,?,?)",
        [name, email, address, contact,city,state]
      );
      employeeData = await executeQuery(
        `select * from schools where id=${employeeData.insertId}`
      );
      res.status(201).json(employeeData);
    }
  } catch (err) {
    res.status(400).json(err);
  }
};

const updateSchool = async (req, res) => {
  let id = req.query.id;
  console.log("id", id);
  const { name, email, address, contact,city,state } = req.body;
  console.log("req.body", req.body);
  try {
    let employeeData = await executeQuery(
      "select * from schools where id=?",
      [id]
    );
    if (employeeData.length > 0) {
      console.log("putrequest", employeeData);
      employeeData = await executeQuery(
        `update schools set name=?,email=?,address=?,contact=?, state=?, city=? where id=${id}`,
        [name, email, address, contact,city,state]
      );
      res.status(200).json(employeeData);
    } else {
      res.status(400).json(`employee not found on this id=${id}`);
    }
  } catch (err) {
    res.status(400).json(err);
  }
};

export {
  getAllSchools,
  getSchoolById,
  deleteSchoolById,
  saveSchool,
  updateSchool,
};
