import Joi from "joi";
const regex = {
  email: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
  onlyNumber: /^\s*-?[0-9]{10}\s*$/,
  onlyAlphabet: /^[A-Za-z ]+$/,
};
const SchoolValidation = (data) => {
  const SchoolSchme = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().required(),
    address: Joi.string().required(),
    contact: Joi.string().required(),
    state: Joi.string().required(),
    city: Joi.string().required(),
  });
  return SchoolSchme.validate(data);
};
// <-------- validate Phone ---------->
export const validatePhone = (phone) => {
  console.log(phone, 'phonev', regex.onlyNumber.test(phone))
  if (regex.onlyNumber.test(phone)) {
    return false;
  } else {
    return true;
  }
};
// <----- validate email ------------->
export const validateEmail = (email) => {
  if (email && email.length < 1) {
    return true;
  } else if (email && regex.email.test(email)) {
    return false;
  } else {
    return true;
  }
};
// <----- validate required ----------->

export const required = (value) => {
  if (!value || !value.toString().trim().length) {
    return true;
  }
  return false;
};
export const validatePercent = (discount) => {
  if (discount && regex.percent.test(discount)) {
    return true;
  } else {
    return false;
  }
};

export const validateOnlyNumber = (number) => {
  if (number && regex.onlyNumber.test(number)) {
    return false;
  } else {
    return true;
  }
};
export const hasValueInObject = (object, value) => {
  if (Object.values(object).includes(value)) {
    return true;
  }
  return false;
};

// To generate 6 digit username that contains only alphabets.
const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
export const GenerateUserName = (length) => {
  let result = '';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export default SchoolValidation;
