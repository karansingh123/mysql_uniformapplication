import React, { useState, useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Card, Button } from 'react-bootstrap';

const SchoolList = () => {
  const [employeeData, setSchoolData] = useState([]);

  useEffect(() => {
    fetch("http://localhost:3000/api/employee")
      .then(response => response.json())
      .then(data => setSchoolData(data))
      .catch(error => console.error('Error fetching data:', error));
  }, []);
  console.log(employeeData)
  return (
    <Container>
      <h2 className='justify-content-center d-flex py-4 mt-5'>Schools List</h2>
      <Row>
        {employeeData.map(employee => (
          <Col key={employee.id}>
            <Card style={{ width: '20rem', height: '30rem', marginBottom: '20px' }}>
              <Card.Img variant="top" src="/Vision.svg" />
              <Card.Body>
                <Card.Text>{employee.city}</Card.Text>
                <Card.Title>{employee.name}</Card.Title>
                <Card.Text>{employee.address}</Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default SchoolList;
