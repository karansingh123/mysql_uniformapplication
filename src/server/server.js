const express = require("express");
const { executeQuery } = require("../config/db");
const port = 9000;
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/employee", async (req, res) => {
  try {
    console.log("dgddddddfgd")
    let employeeData = await executeQuery("select * from schools");
    res.status(200).json(employeeData);
  } catch (err) {
    res.status(500).json(err);
  }
});

app.get("/employee/:id", async (req, res) => {
  let id = req.params.id;
  try {
    let employeeData = await executeQuery(
      "select * from schools where id=?",
      [id]
    );
    res.status(200).json(employeeData);
  } catch (err) {
    res.status(500).json(err);
  }
});

app.post("/saveSchool", async (req, res) => {
  try {
    const { name, email, address, contact,city,state } = req.body;
    let employeeData = await executeQuery(
      "insert into schools(name,email,address,contact,city,state) values(?,?,?,?,?,?)",
      [name, email, address, contact,city,state]
    );
    res.status(201).json(employeeData);
  } catch (err) {
    res.status(400).json(err);
  }
});
app.listen(port, () => console.log(`server is running on port ${port}`));
