import axios from "axios";
import React, { useState } from "react";
import { useRouter } from "next/router";
import styles from "../styles/AddSchool.module.css";
import { Col, Container, Image, Modal, Row } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import { hasValueInObject, required, validateEmail, validatePhone } from "@/common/employeeValidator";
function AddSchool(props) {
  const {
    showModal,
    handleCloseModal, } = props
  const router = useRouter();
  const [addSchool, setSchool] = useState({
    name: "",
    email: "",
    address: "",
    contact: "",
    state: "",
    city: "",
  });
  const onSubmit = async (e) => {
    e.preventDefault();
    const newErrors = findFormErrors()
    if (hasValueInObject(newErrors, true)) {
      newErrors.name == true ? toast.error("Please enter name") :
        newErrors.email == true ? toast.error("Please enter a valid Email") :
          newErrors.address == true ? toast.error("Please enter address") :
            newErrors.contact == true ? toast.error("Please enter phone") :
              newErrors.state == true ? toast.error("Please enter state") :
                newErrors.city == true && toast.error("Please enter city")
    } else {
      let data = await axios.post(
        `http://localhost:3000/api/employee`,
        addSchool
      );
      toast.success('Data Successfully Added', {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
      });
      if (data.data) handleCloseModal(); router.push("/");

      setSchool({
        name: "",
        email: "",
        address: "",
        contact: "",
        state: "",
        city: "",
      });
    };
  }
  const handleChange = (e) => {

    const value = e.target.value;
    console.log("value", value);
    setSchool({ ...addSchool, [e.target.name]: value });
  };

  const findFormErrors = () => {
    const newErrors = {};
    newErrors.name = required(addSchool?.name);
    newErrors.email = required(addSchool?.email);
    newErrors.address = required(addSchool?.address);
    newErrors.contact = required(addSchool?.contact);
    newErrors.state = required(addSchool?.state);
    newErrors.city = required(addSchool?.city);
    return newErrors;
  };
  return (
    <>
     
      <Modal
        show={showModal}
        onHide={handleCloseModal}
        lg="true"
        contentClassName={`${styles.addform}`}
        dialogClassName={``}
      >
        <Modal.Header className={`px-3 pb-0 border-0 d-block d-md-flex justify-content-center justify-content-sm-end`}>
          <Col md={12} className={` d-flex justify-content-end`}>
            <div role="button" onClick={() => handleCloseModal()}>
              <Image src="/modalCrossIcon.svg" height={20} width={20} />
            </div>
          </Col>
        </Modal.Header>
        <Modal.Body className={`px-3 ${styles.scrollarz}`}>
          <Container>
            <form onSubmit={onSubmit}>
              <Row>
                <Col md={12} className={`textStart d-flex justify-content-center `}>
                  <div>
                    <h1>ADD Schools</h1>

                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="name"
                      placeholder="Enter Name"
                      onChange={handleChange}
                      value={addSchool.name}
                    />
                    <label htmlFor="email">Name</label>
                  </div>
                </Col>
                <Col md="6">

                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="email"
                      className={styles.input}
                      name="email"
                      placeholder="Enter Email"
                      onChange={handleChange}
                      value={addSchool.email}
                    />
                    <label htmlFor="email">Email</label>
                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="address"
                      placeholder="Enter Address"
                      onChange={handleChange}
                      value={addSchool.address}
                    />
                    <label htmlFor="email">Address</label>
                  </div>
                </Col>


                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="contact"
                      placeholder="Enter Phone"
                      onChange={handleChange}
                      value={addSchool.contact}
                    />
                    <label htmlFor="email">Phone</label>
                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="state"
                      placeholder="state"
                      onChange={handleChange}
                      value={addSchool.state}
                    />
                    <label htmlFor="email">State</label>
                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      type="text"
                      className={styles.input}
                      name="city"
                      placeholder="City"
                      onChange={handleChange}
                      value={addSchool.city}
                    />
                    <label htmlFor="email">City</label>
                  </div>
                </Col>
                <Col md={12}>
                  <div className="w-100 form-input mb-3">
                    <input
                      id="uploadBtn" type="file" name="image" className={styles.input} multiple="multiple" placeholder="No File Chosen"
                    />
                    {/* <label htmlFor="email"></label> */}
                  </div>
                </Col>

              </Row>
              <div>
                <button type="submit"> Submit</button>
              </div>
            </form>
          </Container>
        </Modal.Body>
      </Modal>
      <ToastContainer />
    </>
  );
}

export default AddSchool;
